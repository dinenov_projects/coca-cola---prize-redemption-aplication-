  
  

## Coca-Cola Prize Redemption Mobile

  ![](https://gitlab.com/Snake_A/coca-cola---prize-redemption-aplication-/raw/Dev/Coca-Cola-Mobile/App_Resources/Android/src/main/res/drawable-hdpi/colabarcode.png)

The mobile application is build with NativeScript 6 and Angular 7.
Currently it supports Android 6+ devices.

The mobile APK can be downloaded here demonstration purposes:
[coca-cola-prize-redemption.APK](https://drive.google.com/open?id=1F7Tla8M_INKTYQ2Gt1ls0NsYR-VPOID2)


## Installation

  
To install the needed dependencies, run this code in the console:
```bash
$ npm install
```

  

## Running the app


To run the application on an Android Virtual Device use the following command:  

```bash
$ tns run android
```

We recommend using  the AVD Manager of Android Studio:
[Android Studio AVD Manager](https://developer.android.com/studio/run/managing-avds)
  
Running a VD requires VT-x to be enabled in the BIOS settings.

### Firebase ML Kit

![enter image description here](https://firebase.google.com/downloads/brand-guidelines/SVG/logo-standard.svg)

The barcode scanner uses the Machine Learning Kit for Firebase:
[ML Kit for Firebase](https://firebase.google.com/products/ml-kit)

Therefore, the first build requires some additional time for the Barcode Scanner to be downloaded and installed on the device

## Authors

#### Dimitar Nenov,

  

#### Alexander Ivanov,


# Structure

  The application contains 4 main components:

## Login screen:

The login screen provides fields for username and password. After tapping on the 'Log in' button, the user will be either sent to the home screen of the application, or will receive an error message: "Wrong username or password!".

## Home page:

The home page contains the main functionality of the application.
Here the user can open the camera barcode scanner to scan the provided barcode. Or they can type the code manually (with a number keyboard).

After tapping on 'Send', the user will be shown a screen, depending on the code status:

### Valid code:

The consumer can choose if to redeem the code and receive the prize, or or to cancel the process. Bot options send a request to the back-end server, adding changing the code status, and updating the Redemption Records.

### Invalid code:

The provided code is not valid.

### Already redeemed:

This screen tells the user that the provided code has already been redeemed. The user can either cancel the process, or report the code, updating the entity and the Redemption Records.

## Records:

Here the user can see chronologically all of their redemptions.

## Support:

This component allows the user to send an email to the support team, using the Android email functionality.
