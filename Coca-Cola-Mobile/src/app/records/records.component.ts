import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { RecordsDataService } from "../core/services/records-data.service";

@Component({
    selector: "Records",
    moduleId: module.id,
    templateUrl: "./records.component.html",
    styleUrls: ["./records.component.css"]
})
export class RecordsComponent implements OnInit {
    standings: Array<any> = [{
        code: "Code",
        status: "Status",
        date: "Date"
    }];

    constructor(
        private recordsService: RecordsDataService
    ) { }

    ngOnInit(): void {
        this.recordsService.getRecords().subscribe((data) => {
            const sorted = data.personal.sort((a, b) => {
                a = new Date(a.dateCreated);
                b = new Date(b.dateCreated);

                return a > b ? -1 : a < b ? 1 : 0;
            });
            sorted.forEach((record) => {
                const date = new Date(record.dateCreated);
                const personal = {
                    code: record.redemptionCode.code,
                    status: record.status,
                    date: date.toLocaleDateString()
                };
                this.standings.push(personal);
            });
        });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
