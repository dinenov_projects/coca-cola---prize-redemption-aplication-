import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { RecordsComponent } from "./records.component";
import { RecordsRoutingModule } from "./records-routing.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { NativeScriptUIDataFormModule } from "nativescript-ui-dataform/angular";

@NgModule({
    imports: [
        NativeScriptUISideDrawerModule,
        NativeScriptUIListViewModule,
        NativeScriptUIDataFormModule,
        NativeScriptCommonModule,
        NativeScriptCommonModule,
        RecordsRoutingModule,
        NativeScriptFormsModule
    ],
    declarations: [
        RecordsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class RecordsModule { }
