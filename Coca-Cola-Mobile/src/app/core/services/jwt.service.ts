import { Injectable } from "@angular/core";
import * as JWT from "jwt-decode";

@Injectable(
    {
        providedIn: "root"
    }
)
export class JWTService {
    JWTDecoder(token: string) {
        return JWT(token);
    }
}
