import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class CodesDataService {
    constructor(private readonly http: HttpClient) { }

    getCodeById(id: string): Observable<any> {
        return this.http.get<any>(`http://10.0.2.2:3000/codes/${id}`);
    }

    updateCode(id: string, status: string): Observable<any> {
        return this.http.put<any>(`http://10.0.2.2:3000/codes/${id}`, { status });

    }

    reportCode(id: string): Observable<any> {
        return this.http.put<any>(`http://10.0.2.2:3000/codes/${id}/report`, {});

    }
}
