import { NgModule, Optional, SkipSelf } from "@angular/core";
import { CommonModule, DecimalPipe } from "@angular/common";
import { StorageService } from "./services/storage.service";
import { JWTService } from "./services/jwt.service";
import { AuthService } from "./services/auth.service";
import { AuthGuardService } from "./services/auth-guard.service";
import { CodesDataService } from "./services/code-data.service";
import { RecordsDataService } from "./services/records-data.service";

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    StorageService,
    JWTService,
    AuthService,
    AuthGuardService,
    CodesDataService,
    RecordsDataService
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error("Core module is already provided!");
    }
  }
}
