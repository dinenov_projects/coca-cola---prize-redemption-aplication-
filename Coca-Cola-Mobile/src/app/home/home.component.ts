import { Component, OnInit, OnDestroy } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import {
    MLKitScanBarcodesResultBarcode,
    MLKitScanBarcodesOnDeviceResult
} from "nativescript-plugin-firebase/mlkit/barcodescanning";
import { CodesDataService } from "../core/services/code-data.service";
import { Subscription } from "rxjs";
import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit, OnDestroy {
    temporaryBarcodes: Array<MLKitScanBarcodesResultBarcode> = [];
    barcodes: Array<MLKitScanBarcodesResultBarcode> = [{
        value: "",
        format: "",
        bounds: {
            origin: { x: 0, y: 0 },
            size: { width: 0, height: 0 }
        },
        image: { width: 0, height: 0 }
    }];
    pause: boolean = false;
    scanning: boolean = false;
    torchOn: boolean = false;
    isValid: boolean = false;
    isRedeemed: boolean = false;
    isInvalid: boolean = false;
    currentCode: string;
    redeemedAt: string;
    redeemedIn: string;
    notAValidCode: boolean = false;

    codeSubscription: Subscription;
    answerSubscription: Subscription;

    constructor(private codesDataService: CodesDataService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onBarcodeScanResult(event: any): void {
        const result: MLKitScanBarcodesOnDeviceResult = event.value;
        this.temporaryBarcodes = result.barcodes;

        if (this.temporaryBarcodes.length > 0) {
            this.barcodes = this.temporaryBarcodes;
            this.pause = true;
            this.scanning = false;
            setTimeout(() => this.pause = false, 10000);
        }
    }

    openScanner() {
        this.scanning = true;
    }

    sendCode(code: string, manual: string) {
        let codeToBeSend: string;
        if (manual) {
            codeToBeSend = manual;
        } else {
            codeToBeSend = code;
        }
        if (+codeToBeSend + 0 !== +codeToBeSend || codeToBeSend.length !== 13) {
            this.notAValidCode = true;
            
            return;
        } else {
            this.notAValidCode = false;
        }
        this.codeSubscription = this.codesDataService.getCodeById(codeToBeSend).subscribe((data) => {
            if (data.status === "valid" || data.status === "canceled") {
                this.isValid = true;
                this.currentCode = data.code;
            }
            if (data.status === "redeemed") {
                this.isRedeemed = true;
                this.redeemedIn = data.redeemedAtOutlet.name;
                this.redeemedAt = data.dateCreated;
                this.currentCode = data.code;
            }
        }, (err) => {
            if (err.status === 404) {
                this.isInvalid = true;
            }
        });
    }

    override(code: string) {
        this.barcodes = [{
            value: code,
            format: "",
            bounds: {
                origin: { x: 0, y: 0 },
                size: { width: 0, height: 0 }
            },
            image: { width: 0, height: 0 }
        }];
    }

    regularCancel() {
        this.isInvalid = false;
        this.isRedeemed = false;
        this.isValid = false;
        this.currentCode = "";
        this.barcodes[0].value = "";
    }

    updateCode(status: string) {
        this.answerSubscription = this.codesDataService.updateCode(this.currentCode, status).subscribe(
            (data) => {
                if (status === "redeemed") {
                    dialogs.alert({
                        title: "Success",
                        message: "Successfully redeemed!",
                        okButtonText: "OK"
                    }).then(() => {
                        this.isInvalid = false;
                        this.isRedeemed = false;
                        this.isValid = false;
                        this.currentCode = "";
                        this.barcodes[0].value = "";
                    });
                } else {
                    this.isInvalid = false;
                    this.isRedeemed = false;
                    this.isValid = false;
                    this.currentCode = "";
                    this.barcodes[0].value = "";
                }
            }, (err) => {
                dialogs.alert({
                    title: "Oops!",
                    message: "Something went wrong!",
                    okButtonText: "OK"
                }).then(() => {
                    this.isInvalid = false;
                    this.isRedeemed = false;
                    this.isValid = false;
                    this.currentCode = "";
                    this.barcodes[0].value = "";
                });
            }
        );
    }

    reportCode() {
        this.answerSubscription = this.codesDataService.reportCode(this.currentCode).subscribe(
            (data) => {
                if (data.reported === true) {
                    dialogs.alert({
                        title: "Success",
                        message: "Successfully reported!",
                        okButtonText: "OK"
                    }).then(() => {
                        this.isInvalid = false;
                        this.isRedeemed = false;
                        this.isValid = false;
                        this.currentCode = "";
                        this.barcodes[0].value = "";
                    });
                } else {
                    this.isInvalid = false;
                    this.isRedeemed = false;
                    this.isValid = false;
                    this.currentCode = "";
                    this.barcodes[0].value = "";
                }
            }, (err) => {
                dialogs.alert({
                    title: "Oops!",
                    message: "Something went wrong!",
                    okButtonText: "OK"
                }).then(() => {
                    this.isInvalid = false;
                    this.isRedeemed = false;
                    this.isValid = false;
                    this.currentCode = "";
                    this.barcodes[0].value = "";
                });
            }
        );
    }

    ngOnDestroy() {
        if (this.codeSubscription) {
            this.codeSubscription.unsubscribe()
        }
        if (this.answerSubscription) {
            this.answerSubscription.unsubscribe()
        }
    }
}
