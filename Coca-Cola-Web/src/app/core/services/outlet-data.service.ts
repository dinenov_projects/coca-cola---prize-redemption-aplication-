import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OutletCreate } from '../../models/outlet-create';
import { OutletEdit } from '../../models/outlet-edit';
import { Outlet } from '../../models/outlet';

@Injectable({
    providedIn: 'root'
})
export class OutletDataService {
    public constructor(private readonly http: HttpClient) { }

    public getAllOutlets(): Observable<Outlet[]> {
        return this.http.get<Outlet[]>(`http://localhost:3000/outlets`);
    }

    public getOutletById(id: string): Observable<Outlet> {
        return this.http.get<Outlet>(`http://localhost:3000/outlets/${id}`);
    }

    public deleteOutlet(id: string): Observable<Outlet> {
        return this.http.delete<Outlet>(`http://localhost:3000/outlets/${id}`);
    }

    public createOutlet(outlet: OutletCreate, customerId: string): Observable<Outlet> {
        return this.http.post<Outlet>(`http://localhost:3000/customers/${customerId}/outlets`, outlet);
    }

    public EditOutlet(id: string, outlet: OutletEdit): Observable<Outlet> {
        return this.http.put<Outlet>(`http://localhost:3000/outlets/${id}`, outlet);
    }
}
