import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Customer } from 'src/app/models/customer';
import { CreateCustomer } from '../../models/create-customer';
import { EditCustomer } from '../../models/edit-customer';

@Injectable({
    providedIn: 'root'
})
export class CustomerDataService {
    public constructor(private readonly http: HttpClient) { }

    public getAllCustomers(): Observable<Customer[]> {
        return this.http.get<Customer[]>(`http://localhost:3000/customers`);
    }

    public getCustomerById(id: string): Observable<Customer> {
        return this.http.get<Customer>(`http://localhost:3000/customers/${id}`);
    }

    public createCustomer(newCustomer: CreateCustomer): Observable<Customer> {
        return this.http.post<Customer>(`http://localhost:3000/customers`, newCustomer);
    }

    public editCustomer(customerId: string, editCustomer: EditCustomer): Observable<EditCustomer> {
        return this.http.put<EditCustomer>(`http://localhost:3000/customers/${customerId}`, editCustomer);
    }

    public deleteCustomer(id: string): Observable<Customer> {
        return this.http.delete<Customer>(`http://localhost:3000/customers/${id}`);
    }
}
