import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Redemption } from 'src/app/models/redemption';

@Injectable({
    providedIn: 'root'
})
export class RedemptionDataService {
    public constructor(private readonly http: HttpClient) { }

    public getAllRedemptions(): Observable<Redemption[]> {
        return this.http.get<Redemption[]>(`http://localhost:3000/records`);
    }
}
