import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { StorageService } from './storage.service';
import { ToastrService } from 'ngx-toastr';
@Injectable()
export class AdminGuardService implements CanActivate {

  constructor(
    private storage: StorageService,
    private router: Router,
    private toastr: ToastrService,
  ) {}

  canActivate(): boolean {
    if (this.storage.get('role') !== 'admin') {
      this.toastr.error(`You do not have access!`);
      this.router.navigate([`/home`]);
      return false;
    }
    return true;
  }
}
