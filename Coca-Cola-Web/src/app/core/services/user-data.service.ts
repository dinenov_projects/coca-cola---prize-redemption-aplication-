import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user';
import { UserRegister } from 'src/app/models/user-register';
import { UserEdit } from 'src/app/models/user-edit';

@Injectable({
    providedIn: 'root'
})
export class UserDataService {
    public constructor(private readonly http: HttpClient) { }

    public getAllUsers(): Observable<User[]> {
        return this.http.get<User[]>(`http://localhost:3000/users`);
    }

    public deleteUser(id: string): Observable<User> {
        return this.http.delete<User>(`http://localhost:3000/users/${id}`);
    }

    public createUser(user: UserRegister): Observable<User> {
        return this.http.post<User>(`http://localhost:3000/users`, user);
    }

    public editUser(id: string, user: UserEdit): Observable<User> {
        return this.http.put<User>(`http://localhost:3000/users/${id}`, user);
    }
}
