import { ErrorHandler, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

    constructor(
    private toastr: ToastrService,
    ) {}

    handleError(error) {
        this.toastr.error('Oops! Something went wrong!');
        console.log(error);
    }
}
