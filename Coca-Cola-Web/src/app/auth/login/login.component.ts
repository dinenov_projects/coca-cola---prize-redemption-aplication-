import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnDestroy {
  public wrongCredentials: any;

  public loginSubscription: Subscription;
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,

  ) { }

  public login(username: string, password: string) {
    this.loginSubscription = this.authService.login(username, password).subscribe((data) => {
      this.router.navigate(['home']);
    }, (err: any) => {
      this.wrongCredentials = 'Wrong Username or Password';
    });
  }

  ngOnDestroy(): void {
    if (this.loginSubscription) {
      this.loginSubscription.unsubscribe();
    }
  }

}
