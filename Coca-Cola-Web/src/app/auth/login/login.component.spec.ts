import { TestBed, async } from '@angular/core/testing';
import { Router } from '@angular/router';
import { CommonModule, DecimalPipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login.component';
import { AuthRoutingModule } from '../auth-routing.module';
import { AuthService } from '../../core/services/auth.service';
import { of } from 'rxjs';
import { asyncError } from 'src/app/testing/async-observable-helpers';


describe('LoginComponent', () => {
    let fixture;
    const router = jasmine.createSpyObj('Router', ['navigate']);
    const authService = jasmine.createSpyObj('AuthService', ['login']);
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                LoginComponent,
            ],
            imports: [
                CommonModule,
                FormsModule,
                AuthRoutingModule,
            ],
            providers: [
                {
                    provide: AuthService,
                    useValue: authService,
                },
                {
                    provide: Router,
                    useValue: router,
                },
                DecimalPipe

            ]
        });
    }));

    afterEach(() => {
        if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
            (fixture.nativeElement as HTMLElement).remove();
        }
    });

    it('should create the component', () => {
        fixture = TestBed.createComponent(LoginComponent);
        const component = fixture.debugElement.componentInstance;
        expect(component).toBeTruthy();
    });

    describe('Login', () => {
        it('login should call login', async () => {
            const username = 'testUser';
            const password = 'testPassword';
            const user = { username, password };

            fixture = TestBed.createComponent(LoginComponent);
            const app = fixture.debugElement.componentInstance;
            authService.login.and.returnValue(of(user));
            app.login(username, password);
            await fixture.detectChanges();
            expect(authService.login).toHaveBeenCalledTimes(1);
        });

        it('login should call refer to /home on success', async () => {
            const username = 'testUser';
            const password = 'testPassword';
            const user = { username, password };

            fixture = TestBed.createComponent(LoginComponent);
            const app = fixture.debugElement.componentInstance;
            authService.login.and.returnValue(of(user));
            const route = ['home'];
            router.navigate.calls.reset();
            app.login(username, password);
            await fixture.detectChanges();
            expect(router.navigate).toHaveBeenCalledTimes(1);
            expect(router.navigate).toHaveBeenCalledWith(route);
        });

        it('login should not refer to /home on fail', async () => {
            const username = 'testUser';
            const password = 'testPassword';
            const user = { username, password };

            const error = new Error('');

            fixture = TestBed.createComponent(LoginComponent);
            const app = fixture.debugElement.componentInstance;
            authService.login.and.returnValue(asyncError(error));
            router.navigate.calls.reset();
            await fixture.detectChanges();
            app.login(username, password);
            expect(router.navigate).toHaveBeenCalledTimes(0);
        });
    });
});
