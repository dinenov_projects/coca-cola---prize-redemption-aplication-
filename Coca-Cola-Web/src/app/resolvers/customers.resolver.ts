import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { CustomerDataService } from '../core/services/customer-data.service';
import { Observable } from 'rxjs';
import { Customer } from '../models/customer';

@Injectable()
export class CustomersResolver implements Resolve<Observable<Customer[]>> {

  constructor(private customerDataService: CustomerDataService) {}

  resolve() {
      return this.customerDataService.getAllCustomers();
  }
}
