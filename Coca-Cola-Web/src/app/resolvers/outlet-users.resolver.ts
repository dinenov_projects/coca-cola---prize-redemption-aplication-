import { Injectable, OnDestroy } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { OutletDataService } from '../core/services/outlet-data.service';
import { Observable, of, Subscription } from 'rxjs';
import { Outlet } from '../models/outlet';
import { User } from '../models/user';

@Injectable()
export class OutletUsersResolver implements Resolve<Observable<User[]>>, OnDestroy {
  private outletSubscription: Subscription;

  constructor(private outletDataService: OutletDataService) {}

  resolve(route: ActivatedRouteSnapshot) {
    let users: User[];
    this.outletSubscription = this.outletDataService.getOutletById(route.params.id).subscribe(
      (data: Outlet) => {
        users = data.users;
      }, err => {}
    );
    return of(users);
  }

  ngOnDestroy() {
    this.outletSubscription.unsubscribe();
  }
}
