import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { OutletDataService } from '../core/services/outlet-data.service';
import { Observable } from 'rxjs';
import { Outlet } from '../models/outlet';

@Injectable()
export class OutletsResolver implements Resolve<Observable<Outlet[]>> {

  constructor(private outletDataService: OutletDataService) {}

  resolve() {
    return this.outletDataService.getAllOutlets();
  }
}
