import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MustMatch } from '../../validators/must-match.validator';
import { Outlet } from '../../models/outlet';
import { UserRegister } from '../../models/user-register';
import { CustomValidators } from '../../validators/register-validator';

@Component({
  selector: 'app-user-create',
  templateUrl: 'user-create.component.html',
  styleUrls: ['user-create.component.css']
})
export class UserCreateComponent implements OnInit {
  @Input()
  outletsResolve: Outlet[];
  @Output()
  userCreatedEvent = new EventEmitter<UserRegister>();
  registerForm: FormGroup;
  submitted = false;
  singleSelect: Outlet;
  config = {
    displayKey: 'name',
    search: true,
    limitTo: 30,
  };
  options: Outlet[] = [];

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.options = this.outletsResolve;
    this.registerForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.minLength(2)]],
      username: ['', [Validators.required, Validators.minLength(2)]],
      password: ['', [Validators.required, Validators.minLength(6),
        CustomValidators.patternValidator(/\d/, {
        hasNumber: true
      }),
      CustomValidators.patternValidator(/[A-Z]/, {
        hasCapitalCase: true
      }),
      CustomValidators.patternValidator(/[a-z]/, {
        hasSmallCase: true
      }),
      CustomValidators.patternValidator(
        /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
        {
          hasSpecialCharacters: true
        }
      ),]],
      confirmPassword: ['', [Validators.required]]
    }, {
        validator: MustMatch('password', 'confirmPassword')
      });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    const user: UserRegister = {
      username: this.registerForm.value.username,
      firstName: this.registerForm.value.firstName,
      lastName: this.registerForm.value.lastName,
      password: this.registerForm.value.password,
      outletId: this.singleSelect.id,
    };

    this.userCreatedEvent.emit(user);
  }

}
