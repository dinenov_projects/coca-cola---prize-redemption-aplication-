import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../models/user';
import { UserDataService } from '../core/services/user-data.service';
import { Subscription } from 'rxjs';
import { Outlet } from '../models/outlet';
import { UserRegister } from '../models/user-register';
import { UserEdit } from '../models/user-edit';
import { ToastrService } from 'ngx-toastr';
import { UserService } from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {

  users: User[] = [];
  outletsResolve: Outlet[] = [];
  private usersSubscription: Subscription;
  private usersDeleteSubscription: Subscription;
  private usersCreateSubscription: Subscription;
  private usersEditSubscription: Subscription;

  constructor(
    private userDataService: UserDataService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.usersSubscription = this.route.data.subscribe(
      (users: any) => {
        const data = this.userService.getData(users, this.route);
        this.users = data.users;
        this.outletsResolve = data.outletsResolve;
      }
    );
  }

  deleteUser(userId: string) {
    this.usersDeleteSubscription = this.userDataService.deleteUser(userId).subscribe((data) => {
      const deletedIndex = this.users.findIndex((user: User) => user.id === userId);
      const users = this.users.slice();
      users.splice(deletedIndex, 1);
      this.users = users;
      this.toastr.success('User deleted successfully!');
    }, err => { });
  }

  createUser(user: UserRegister) {
    this.usersCreateSubscription = this.userDataService.createUser(user).subscribe(
      (data) => {
        this.users = [...this.users, data];
        this.toastr.success('User created successfully!');
      }
    );
  }

  editUser(newData: { user: UserEdit, id: string }) {
    this.usersEditSubscription = this.userDataService.editUser(newData.id, newData.user).subscribe(
      (data) => {
        const userFound: number = this.users.findIndex((editedUser: User) => editedUser.id === data.id);
        const newUsers: User[] = this.users.slice();
        newUsers[userFound] = data;
        this.users = newUsers;
        this.toastr.success('User edited successfully!');
      }, err => {}
    );
  }

  ngOnDestroy() {
    if (this.usersSubscription) {
      this.usersSubscription.unsubscribe();
    }
    if (this.usersDeleteSubscription) {
      this.usersDeleteSubscription.unsubscribe();
    }
    if (this.usersCreateSubscription) {
      this.usersCreateSubscription.unsubscribe();
    }
    if (this.usersEditSubscription) {
      this.usersEditSubscription.unsubscribe();
    }
  }


}
