import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { UserRoutingModule } from './user-routing.module';
import { SharedModule } from '../shared/shared.module';
import { UserTableComponent } from './user-table/user-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserCreateComponent } from './user-create/user-create.component';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserService } from './user.service';

@NgModule({
  declarations: [UserComponent, UserTableComponent, UserCreateComponent, UserEditComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    SelectDropDownModule,
  ],
  providers: [UserService],
})
export class UserModule { }
