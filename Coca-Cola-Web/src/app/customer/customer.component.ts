import { CreateCustomer } from './../models/create-customer';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Customer } from '../models/customer';
import { CustomerDataService } from '../core/services/customer-data.service';
import { Subscription } from 'rxjs';
import { EditCustomer } from '../models/edit-customer';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit, OnDestroy {

  customers: Customer[] = [];
  private customersSubscription: Subscription;
  createButton = false;
  editButton = false;
  customerID: string;
  input: string;
  editContent: string;
  private customersDeleteSubscription: Subscription;
  private customersCreateSubscription: Subscription;
  private customersEditSubscription: Subscription;

  constructor(
    private customerDataService: CustomerDataService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
  ) { }

  toggleButton(): void {
    this.createButton = !this.createButton;
  }

  ngOnInit() {
    this.customersSubscription = this.route.data.subscribe(
      (customers: { customers: Customer[] }) => {
        this.customers = customers.customers;
      });
  }

  createCustomer(): void {
    const newCustomer: CreateCustomer = {
      name: this.input
    };
    this.customersCreateSubscription = this.customerDataService.createCustomer(newCustomer).subscribe((res: Customer) => {
      this.customers = [...this.customers, res];
      this.toastr.success('Customer created successfully!');
    });
    this.createButton = !this.createButton;
  }

  toggleButtonEdit(content: {
    status: boolean,
    id: string,
  }): void {
    this.editButton = content.status;
    this.customerID = content.id;
  }

  editCustomer(): void {
    const editedCustomer: EditCustomer = {
      name: this.editContent,
    };
    const customerId = this.customerID;
    this.customersEditSubscription = this.customerDataService.editCustomer(customerId, editedCustomer).subscribe((res) => {
      const customerToEdit = this.customers.find((customer) => customer.id === this.customerID);
      customerToEdit.name = editedCustomer.name;
      this.toastr.success('Customer edited successfully!');
    });
    this.editButton = !this.editButton;
  }

  deleteCustomer(customerId: string) {
    this.customersDeleteSubscription = this.customerDataService.deleteCustomer(customerId).subscribe((data) => {
      const deletedIndex = this.customers.findIndex((customer: Customer) => customer.id === customerId);
      const customers = this.customers.slice();
      customers.splice(deletedIndex, 1);
      this.customers = customers;
      this.toastr.success('Customer deleted successfully!');
    });
  }

  ngOnDestroy() {
    if (this.customersSubscription) {
      this.customersSubscription.unsubscribe();
    }
    if (this.customersDeleteSubscription) {
      this.customersDeleteSubscription.unsubscribe();
    }
    if (this.customersCreateSubscription) {
      this.customersCreateSubscription.unsubscribe();
    }
    if (this.customersEditSubscription) {
      this.customersEditSubscription.unsubscribe();
    }
  }

}
