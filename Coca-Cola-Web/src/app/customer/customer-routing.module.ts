import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CustomerComponent } from './customer.component';

export const routes: Routes = [
    { path: '', component: CustomerComponent, pathMatch: 'full' },
    {
      path: ':id/outlets', loadChildren: '../outlet/outlet.module#OutletModule',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class CustomerRoutingModule {}
