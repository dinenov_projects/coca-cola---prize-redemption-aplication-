import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Outlet } from '../models/outlet';
import { OutletDataService } from '../core/services/outlet-data.service';
import { Subscription } from 'rxjs';
import { Customer } from '../models/customer';
import { OutletEdit } from '../models/outlet-edit';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-outlet',
  templateUrl: './outlet.component.html',
  styleUrls: ['./outlet.component.css']
})
export class OutletComponent implements OnInit, OnDestroy {

  customers: Customer[];
  private customerSubscription: Subscription;
  outlets: Outlet[] = [];
  private outletsSubscription: Subscription;
  private outletsDeleteSubscription: Subscription;
  private outletsCreateSubscription: Subscription;
  private outletsEditSubscription: Subscription;

  constructor(
    private outletDataService: OutletDataService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.outletsSubscription = this.route.data.subscribe(
      (outlets: any) => {
        if (outlets.outlets) {
          this.outlets = outlets.outlets;
          this.customers = outlets.customers;
        } else if (outlets.customers) {
          const customer = outlets.customers.find((outlet: Outlet) => outlet.id === this.route.snapshot.params.id);
          this.outlets = customer.outlets;
          this.outlets.forEach((outlet: Outlet) => {
            outlet.customer = customer;
          });
          this.customers = outlets.customers;
        }
      }, err => { }
    );
    this.customerSubscription = this.route.data.subscribe((customers: any) => {
      this.customers = customers;
    });
  }

  deleteOutlet(outletId: string) {
    this.outletsDeleteSubscription = this.outletDataService.deleteOutlet(outletId).subscribe((data) => {
      const deletedIndex = this.outlets.findIndex((outlet: Outlet) => outlet.id === outletId);
      const outlets = this.outlets.slice();
      outlets.splice(deletedIndex, 1);
      this.outlets = outlets;
      this.toastr.success('Outlet deleted successfully!');
    }, err => { });
  }

  createOutlet(createData) {
    this.outletsCreateSubscription = this.outletDataService.createOutlet(createData.outlet, createData.customerId).subscribe(
      (data) => {
        this.outlets = [...this.outlets, data];
        this.toastr.success('Outlet created successfully!');
      }
    );
  }

  editOutlet(newData: { outlet: OutletEdit, id: string }) {
    this.outletsEditSubscription = this.outletDataService.EditOutlet(newData.id, newData.outlet).subscribe(
      (data) => {
        const outletFound: number = this.outlets.findIndex((editedOutlet: Outlet) => editedOutlet.id === data.id);
        const newOutlets: Outlet[] = this.outlets.slice();
        newOutlets[outletFound] = data;
        this.outlets = newOutlets;
        this.toastr.success('Outlet edited successfully!');
      }
    );
  }

  ngOnDestroy() {
    if (this.outletsSubscription) {
      this.outletsSubscription.unsubscribe();
    }
    if (this.outletsDeleteSubscription) {
      this.outletsDeleteSubscription.unsubscribe();
    }
    if (this.outletsCreateSubscription) {
      this.outletsCreateSubscription.unsubscribe();
    }
    if (this.outletsEditSubscription) {
      this.outletsEditSubscription.unsubscribe();
    }
    if (this.customerSubscription) {
      this.customerSubscription.unsubscribe();
    }
  }
}
