import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { OutletComponent } from './outlet.component';

const routes: Routes = [
  { path: '', component: OutletComponent, pathMatch: 'full' },
  {
    path: ':id/users', loadChildren: '../user/user.module#UserModule',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OutletRoutingModule { }
