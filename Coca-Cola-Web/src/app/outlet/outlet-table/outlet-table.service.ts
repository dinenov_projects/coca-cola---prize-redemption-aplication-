import { Injectable, PipeTransform } from '@angular/core';

import { BehaviorSubject, Observable, of, Subject } from 'rxjs';

import { DecimalPipe } from '@angular/common';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { Outlet } from 'src/app/models/outlet';
import { SortDirection } from '../../directives/sortable.directive';

interface SearchResult {
    outlets: Outlet[];
    total: number;
}

interface State {
    page: number;
    pageSize: number;
    searchTerm: string;
    sortColumn: string;
    sortDirection: SortDirection;
    sub: string;
}

function compare(v1, v2) {
    return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(outlets: Outlet[], column: string, direction: string, sub: string): Outlet[] {
    if (direction === '') {
        return outlets;
    } else {
        return [...outlets].sort((a, b) => {
            if (sub) {
                const res1 = compare(a[column][sub], b[column][sub]);
                return direction === 'asc' ? res1 : -res1;
            }
            const res = compare(a[column], b[column]);
            return direction === 'asc' ? res : -res;
        });
    }
}

function matches(outlet: Outlet, term: string, pipe: PipeTransform) {
    return outlet.name.toLowerCase().includes(term.toLowerCase())
        || outlet.id.toLowerCase().includes(term.toLowerCase())
        || outlet.address.toLowerCase().includes(term.toLowerCase())
        || outlet.customer.name.toLowerCase().includes(term.toLowerCase());
}

@Injectable({ providedIn: 'root' })
export class OutletTableService {
    // tslint:disable: variable-name
    private _loading$ = new BehaviorSubject<boolean>(true);
    private _search$ = new Subject<void>();
    private _outlets$ = new BehaviorSubject<Outlet[]>([]);
    private _total$ = new BehaviorSubject<number>(0);
    private _outlets: Outlet[];

    private _state: State = {
        page: 1,
        pageSize: 10,
        searchTerm: '',
        sortColumn: '',
        sortDirection: '',
        sub: ''
    };

    constructor(private pipe: DecimalPipe) {
        this._search$.pipe(
            tap(() => this._loading$.next(true)),
            debounceTime(200),
            switchMap(() => this._search()),
            delay(200),
            tap(() => this._loading$.next(false))
        ).subscribe(result => {
            this._outlets$.next(result.outlets);
            this._total$.next(result.total);
        }, err => { });

        this._search$.next();
    }

    get outlets$() { return this._outlets$.asObservable(); }

    get total$() { return this._total$.asObservable(); }

    get loading$() { return this._loading$.asObservable(); }

    get page() { return this._state.page; }
    set page(page: number) { this._set({ page }); }

    get pageSize() { return this._state.pageSize; }
    set pageSize(pageSize: number) { this._set({ pageSize }); }

    get searchTerm() { return this._state.searchTerm; }
    set searchTerm(searchTerm: string) { this._set({ searchTerm }); }

    set sortColumn(sortColumn: string) { this._set({ sortColumn }); }

    set sortDirection(sortDirection: SortDirection) { this._set({ sortDirection }); }

    set sub(sub: string) { this._set({ sub }); }

    set outlets(outlets: Outlet[]) { this._outlets = outlets; }

    reset() {
        this._search$.pipe(
            tap(() => this._loading$.next(true)),
            debounceTime(200),
            switchMap(() => this._search()),
            delay(200),
            tap(() => this._loading$.next(false))
        ).subscribe(result => {
            this._outlets$.next(result.outlets);
            this._total$.next(result.total);
        }, err => { });

        this._search$.next();
    }

    private _set(patch: Partial<State>) {
        Object.assign(this._state, patch);
        this._search$.next();
    }

    private _search(): Observable<SearchResult> {
        const { sortColumn, sortDirection, pageSize, page, searchTerm, sub } = this._state;

        // 1. sort
        let outlets = sort(this._outlets, sortColumn, sortDirection, sub);

        // 2. filter
        outlets = outlets.filter(outlet => matches(outlet, searchTerm, this.pipe));
        const total = outlets.length;

        // 3. paginate
        outlets = outlets.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
        return of({ outlets, total });
    }
}
