export interface UserRegister {

    username: string;

    password: string;

    firstName: string;

    lastName: string;

    outletId: string;
}
