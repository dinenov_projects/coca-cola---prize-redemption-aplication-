import { User } from './user';
import { Customer } from './customer';
import { Outlet } from './outlet';
import { RedemptionCodeStatus } from './enums/redemption-code-status-type';
import { RedemptionCode } from './redemption-code';

export interface Redemption {
    redemptionCode: RedemptionCode;
    updatedByUser: User;
    customer: Customer;
    outlet: Outlet;
    dateCreated: Date;
    status: RedemptionCodeStatus;
    info: string;
}
