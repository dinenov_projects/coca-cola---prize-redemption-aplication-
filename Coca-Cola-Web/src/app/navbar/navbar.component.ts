import { AuthService } from './../core/services/auth.service';
import {
  Component,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from '../core/services/storage.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {
  showLogin: boolean;
  userSubscription: Subscription;

  collapsed = true;

  constructor(
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly storageService: StorageService,
  ) { }

  public redirectToCustomers(): void {
    this.router.navigate(['/customers']);
  }

  public redirectToLogin(): void {
    this.router.navigate(['/login']);
  }

  public logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  toggleCollapsed(): void {
    this.collapsed = !this.collapsed;
  }

  ngOnInit() {
    this.userSubscription = this.authService.user$.subscribe(
      (username: string) => {
        if (!username) {
          this.showLogin = true;
        } else {
          this.showLogin = false;
        }
      }
    );

  }

  ngOnDestroy() {
  }

}
