import { Outlet } from './outlet.entity';
import { Customer } from './customer.entity';
import { User } from './user.entity';
import { PrimaryGeneratedColumn, Column, Entity, ManyToOne } from 'typeorm';
import { RedemptionCode } from './redemption-code.entity';
import { RedemptionCodeStatus } from '../../common/enums/redemption-code-status-type';

@Entity('redemption-record')
export class RedemptionRecord {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @ManyToOne(type => RedemptionCode, redemptionCode => redemptionCode.redemptionRecord)
    redemptionCode: Promise<RedemptionCode>;

    @ManyToOne(type => User, user => user.redeptionRecords)
    updatedByUser: Promise<User>;

    @ManyToOne(type => Customer, customer => customer.redemptionRecords)
    customer: Promise<Customer>;

    @ManyToOne(type => Outlet, outlet => outlet.redemptionRecords)
    outlet: Promise<Outlet>;

    @Column()
    dateCreated: Date;

    @Column()
    status: RedemptionCodeStatus;

    @Column()
    info: string;
}
