import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, OneToMany, OneToOne, ManyToOne } from 'typeorm';
import { User } from './user.entity';
import { Customer } from './customer.entity';
import { RedemptionCode } from './redemption-code.entity';
import { RedemptionRecord } from './redemption-record.entity';

@Entity('outlet')
export class Outlet {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('nvarchar', {
        unique: true,
    })
    name: string;

    @Column('nvarchar', {
        unique: true,
    })
    address: string;

    @ManyToOne(type => Customer, customer => customer.outlets)
    customer: Promise<Customer>;

    @OneToMany(type => User, users => users.outlet)
    users: Promise<User[]>;

    @OneToMany(type => RedemptionCode, redemptionCodes => redemptionCodes.redeemedAtOutlet)
    redemptionCodes: Promise<RedemptionCode[]>;

    @OneToMany(type => RedemptionRecord, redemptionRecord => redemptionRecord.outlet)
    redemptionRecords: Promise<RedemptionRecord[]>;

    @CreateDateColumn()
    dateCreated: string;

    @Column({default: false})
    isDeleted: boolean;
}
