import {MigrationInterface, QueryRunner} from "typeorm";

export class Init1564317564031 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `blacklist` (`id` int NOT NULL AUTO_INCREMENT, `token` varchar(255) NOT NULL, `date` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `redemption-code` (`id` varchar(36) NOT NULL, `code` varchar(255) NOT NULL, `status` varchar(255) NOT NULL DEFAULT 'valid', `redeemedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `dateCreated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `reported` tinyint NOT NULL DEFAULT 0, `redeemedAtOutletId` varchar(36) NULL, `redeemedFromId` varchar(36) NULL, `redeemedFromCustomerId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `redemption-record` (`id` varchar(36) NOT NULL, `dateCreated` datetime NOT NULL, `status` varchar(255) NOT NULL, `info` varchar(255) NOT NULL, `redemptionCodeId` varchar(36) NULL, `updatedByUserId` varchar(36) NULL, `customerId` varchar(36) NULL, `outletId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `roles` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user` (`id` varchar(36) NOT NULL, `username` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `firstName` varchar(255) NOT NULL, `lastName` varchar(255) NOT NULL, `dateCreated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL DEFAULT 0, `customerId` varchar(36) NULL, `outletId` varchar(36) NULL, UNIQUE INDEX `IDX_78a916df40e02a9deb1c4b75ed` (`username`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `outlet` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `address` varchar(255) NOT NULL, `dateCreated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL DEFAULT 0, `customerId` varchar(36) NULL, UNIQUE INDEX `IDX_6e932841d15452c92572cce298` (`name`), UNIQUE INDEX `IDX_3c4ced2bf24ac97ca04bdc1efa` (`address`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `customer` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `dateCreated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL DEFAULT 0, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user_roles_roles` (`userId` varchar(36) NOT NULL, `rolesId` int NOT NULL, INDEX `IDX_0d0cc409255467b0ac4fe6b169` (`userId`), INDEX `IDX_7521d8491e7c51f885e9f861e0` (`rolesId`), PRIMARY KEY (`userId`, `rolesId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `redemption-code` ADD CONSTRAINT `FK_990f43507fcc0511a36815fd880` FOREIGN KEY (`redeemedAtOutletId`) REFERENCES `outlet`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-code` ADD CONSTRAINT `FK_8d41fc2b1112d8942908dc1c3c9` FOREIGN KEY (`redeemedFromId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-code` ADD CONSTRAINT `FK_0840600516d67cb748148941a6a` FOREIGN KEY (`redeemedFromCustomerId`) REFERENCES `customer`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-record` ADD CONSTRAINT `FK_20a56d9b931eb37e65388fc294a` FOREIGN KEY (`redemptionCodeId`) REFERENCES `redemption-code`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-record` ADD CONSTRAINT `FK_a1b61f7cbe7b3aaf60e72fee444` FOREIGN KEY (`updatedByUserId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-record` ADD CONSTRAINT `FK_698c80f6014b82982f2766c2922` FOREIGN KEY (`customerId`) REFERENCES `customer`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-record` ADD CONSTRAINT `FK_87eaa9b3ba2c8823579fd66ce43` FOREIGN KEY (`outletId`) REFERENCES `outlet`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user` ADD CONSTRAINT `FK_6c687a8fa35b0ae35ce766b56ce` FOREIGN KEY (`customerId`) REFERENCES `customer`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user` ADD CONSTRAINT `FK_3d3b1fbedf229c3994b68a477fe` FOREIGN KEY (`outletId`) REFERENCES `outlet`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `outlet` ADD CONSTRAINT `FK_cce20aa27a650db8cfc6db45d58` FOREIGN KEY (`customerId`) REFERENCES `customer`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user_roles_roles` ADD CONSTRAINT `FK_0d0cc409255467b0ac4fe6b1693` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user_roles_roles` ADD CONSTRAINT `FK_7521d8491e7c51f885e9f861e02` FOREIGN KEY (`rolesId`) REFERENCES `roles`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `user_roles_roles` DROP FOREIGN KEY `FK_7521d8491e7c51f885e9f861e02`");
        await queryRunner.query("ALTER TABLE `user_roles_roles` DROP FOREIGN KEY `FK_0d0cc409255467b0ac4fe6b1693`");
        await queryRunner.query("ALTER TABLE `outlet` DROP FOREIGN KEY `FK_cce20aa27a650db8cfc6db45d58`");
        await queryRunner.query("ALTER TABLE `user` DROP FOREIGN KEY `FK_3d3b1fbedf229c3994b68a477fe`");
        await queryRunner.query("ALTER TABLE `user` DROP FOREIGN KEY `FK_6c687a8fa35b0ae35ce766b56ce`");
        await queryRunner.query("ALTER TABLE `redemption-record` DROP FOREIGN KEY `FK_87eaa9b3ba2c8823579fd66ce43`");
        await queryRunner.query("ALTER TABLE `redemption-record` DROP FOREIGN KEY `FK_698c80f6014b82982f2766c2922`");
        await queryRunner.query("ALTER TABLE `redemption-record` DROP FOREIGN KEY `FK_a1b61f7cbe7b3aaf60e72fee444`");
        await queryRunner.query("ALTER TABLE `redemption-record` DROP FOREIGN KEY `FK_20a56d9b931eb37e65388fc294a`");
        await queryRunner.query("ALTER TABLE `redemption-code` DROP FOREIGN KEY `FK_0840600516d67cb748148941a6a`");
        await queryRunner.query("ALTER TABLE `redemption-code` DROP FOREIGN KEY `FK_8d41fc2b1112d8942908dc1c3c9`");
        await queryRunner.query("ALTER TABLE `redemption-code` DROP FOREIGN KEY `FK_990f43507fcc0511a36815fd880`");
        await queryRunner.query("DROP INDEX `IDX_7521d8491e7c51f885e9f861e0` ON `user_roles_roles`");
        await queryRunner.query("DROP INDEX `IDX_0d0cc409255467b0ac4fe6b169` ON `user_roles_roles`");
        await queryRunner.query("DROP TABLE `user_roles_roles`");
        await queryRunner.query("DROP TABLE `customer`");
        await queryRunner.query("DROP INDEX `IDX_3c4ced2bf24ac97ca04bdc1efa` ON `outlet`");
        await queryRunner.query("DROP INDEX `IDX_6e932841d15452c92572cce298` ON `outlet`");
        await queryRunner.query("DROP TABLE `outlet`");
        await queryRunner.query("DROP INDEX `IDX_78a916df40e02a9deb1c4b75ed` ON `user`");
        await queryRunner.query("DROP TABLE `user`");
        await queryRunner.query("DROP TABLE `roles`");
        await queryRunner.query("DROP TABLE `redemption-record`");
        await queryRunner.query("DROP TABLE `redemption-code`");
        await queryRunner.query("DROP TABLE `blacklist`");
    }

}
