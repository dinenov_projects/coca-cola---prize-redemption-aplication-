import {MigrationInterface, QueryRunner} from "typeorm";

export class LongtextAdded1564320074475 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `blacklist` DROP COLUMN `token`");
        await queryRunner.query("ALTER TABLE `blacklist` ADD `token` longtext NOT NULL");
        await queryRunner.query("ALTER TABLE `blacklist` CHANGE `date` `date` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `redemption-code` DROP FOREIGN KEY `FK_990f43507fcc0511a36815fd880`");
        await queryRunner.query("ALTER TABLE `redemption-code` DROP FOREIGN KEY `FK_8d41fc2b1112d8942908dc1c3c9`");
        await queryRunner.query("ALTER TABLE `redemption-code` DROP FOREIGN KEY `FK_0840600516d67cb748148941a6a`");
        await queryRunner.query("ALTER TABLE `redemption-code` CHANGE `redeemedAt` `redeemedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `redemption-code` CHANGE `dateCreated` `dateCreated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `redemption-code` CHANGE `redeemedAtOutletId` `redeemedAtOutletId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `redemption-code` CHANGE `redeemedFromId` `redeemedFromId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `redemption-code` CHANGE `redeemedFromCustomerId` `redeemedFromCustomerId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `redemption-record` DROP FOREIGN KEY `FK_20a56d9b931eb37e65388fc294a`");
        await queryRunner.query("ALTER TABLE `redemption-record` DROP FOREIGN KEY `FK_a1b61f7cbe7b3aaf60e72fee444`");
        await queryRunner.query("ALTER TABLE `redemption-record` DROP FOREIGN KEY `FK_698c80f6014b82982f2766c2922`");
        await queryRunner.query("ALTER TABLE `redemption-record` DROP FOREIGN KEY `FK_87eaa9b3ba2c8823579fd66ce43`");
        await queryRunner.query("ALTER TABLE `redemption-record` CHANGE `redemptionCodeId` `redemptionCodeId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `redemption-record` CHANGE `updatedByUserId` `updatedByUserId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `redemption-record` CHANGE `customerId` `customerId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `redemption-record` CHANGE `outletId` `outletId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `user` DROP FOREIGN KEY `FK_6c687a8fa35b0ae35ce766b56ce`");
        await queryRunner.query("ALTER TABLE `user` DROP FOREIGN KEY `FK_3d3b1fbedf229c3994b68a477fe`");
        await queryRunner.query("ALTER TABLE `user` CHANGE `dateCreated` `dateCreated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `user` CHANGE `customerId` `customerId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `outletId` `outletId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `outlet` DROP FOREIGN KEY `FK_cce20aa27a650db8cfc6db45d58`");
        await queryRunner.query("ALTER TABLE `outlet` CHANGE `dateCreated` `dateCreated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `outlet` CHANGE `customerId` `customerId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `customer` CHANGE `dateCreated` `dateCreated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `redemption-code` ADD CONSTRAINT `FK_990f43507fcc0511a36815fd880` FOREIGN KEY (`redeemedAtOutletId`) REFERENCES `outlet`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-code` ADD CONSTRAINT `FK_8d41fc2b1112d8942908dc1c3c9` FOREIGN KEY (`redeemedFromId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-code` ADD CONSTRAINT `FK_0840600516d67cb748148941a6a` FOREIGN KEY (`redeemedFromCustomerId`) REFERENCES `customer`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-record` ADD CONSTRAINT `FK_20a56d9b931eb37e65388fc294a` FOREIGN KEY (`redemptionCodeId`) REFERENCES `redemption-code`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-record` ADD CONSTRAINT `FK_a1b61f7cbe7b3aaf60e72fee444` FOREIGN KEY (`updatedByUserId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-record` ADD CONSTRAINT `FK_698c80f6014b82982f2766c2922` FOREIGN KEY (`customerId`) REFERENCES `customer`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-record` ADD CONSTRAINT `FK_87eaa9b3ba2c8823579fd66ce43` FOREIGN KEY (`outletId`) REFERENCES `outlet`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user` ADD CONSTRAINT `FK_6c687a8fa35b0ae35ce766b56ce` FOREIGN KEY (`customerId`) REFERENCES `customer`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user` ADD CONSTRAINT `FK_3d3b1fbedf229c3994b68a477fe` FOREIGN KEY (`outletId`) REFERENCES `outlet`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `outlet` ADD CONSTRAINT `FK_cce20aa27a650db8cfc6db45d58` FOREIGN KEY (`customerId`) REFERENCES `customer`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `outlet` DROP FOREIGN KEY `FK_cce20aa27a650db8cfc6db45d58`");
        await queryRunner.query("ALTER TABLE `user` DROP FOREIGN KEY `FK_3d3b1fbedf229c3994b68a477fe`");
        await queryRunner.query("ALTER TABLE `user` DROP FOREIGN KEY `FK_6c687a8fa35b0ae35ce766b56ce`");
        await queryRunner.query("ALTER TABLE `redemption-record` DROP FOREIGN KEY `FK_87eaa9b3ba2c8823579fd66ce43`");
        await queryRunner.query("ALTER TABLE `redemption-record` DROP FOREIGN KEY `FK_698c80f6014b82982f2766c2922`");
        await queryRunner.query("ALTER TABLE `redemption-record` DROP FOREIGN KEY `FK_a1b61f7cbe7b3aaf60e72fee444`");
        await queryRunner.query("ALTER TABLE `redemption-record` DROP FOREIGN KEY `FK_20a56d9b931eb37e65388fc294a`");
        await queryRunner.query("ALTER TABLE `redemption-code` DROP FOREIGN KEY `FK_0840600516d67cb748148941a6a`");
        await queryRunner.query("ALTER TABLE `redemption-code` DROP FOREIGN KEY `FK_8d41fc2b1112d8942908dc1c3c9`");
        await queryRunner.query("ALTER TABLE `redemption-code` DROP FOREIGN KEY `FK_990f43507fcc0511a36815fd880`");
        await queryRunner.query("ALTER TABLE `customer` CHANGE `dateCreated` `dateCreated` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("ALTER TABLE `outlet` CHANGE `customerId` `customerId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `outlet` CHANGE `dateCreated` `dateCreated` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("ALTER TABLE `outlet` ADD CONSTRAINT `FK_cce20aa27a650db8cfc6db45d58` FOREIGN KEY (`customerId`) REFERENCES `customer`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user` CHANGE `outletId` `outletId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `user` CHANGE `customerId` `customerId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `user` CHANGE `dateCreated` `dateCreated` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("ALTER TABLE `user` ADD CONSTRAINT `FK_3d3b1fbedf229c3994b68a477fe` FOREIGN KEY (`outletId`) REFERENCES `outlet`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user` ADD CONSTRAINT `FK_6c687a8fa35b0ae35ce766b56ce` FOREIGN KEY (`customerId`) REFERENCES `customer`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-record` CHANGE `outletId` `outletId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `redemption-record` CHANGE `customerId` `customerId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `redemption-record` CHANGE `updatedByUserId` `updatedByUserId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `redemption-record` CHANGE `redemptionCodeId` `redemptionCodeId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `redemption-record` ADD CONSTRAINT `FK_87eaa9b3ba2c8823579fd66ce43` FOREIGN KEY (`outletId`) REFERENCES `outlet`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-record` ADD CONSTRAINT `FK_698c80f6014b82982f2766c2922` FOREIGN KEY (`customerId`) REFERENCES `customer`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-record` ADD CONSTRAINT `FK_a1b61f7cbe7b3aaf60e72fee444` FOREIGN KEY (`updatedByUserId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-record` ADD CONSTRAINT `FK_20a56d9b931eb37e65388fc294a` FOREIGN KEY (`redemptionCodeId`) REFERENCES `redemption-code`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-code` CHANGE `redeemedFromCustomerId` `redeemedFromCustomerId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `redemption-code` CHANGE `redeemedFromId` `redeemedFromId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `redemption-code` CHANGE `redeemedAtOutletId` `redeemedAtOutletId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `redemption-code` CHANGE `dateCreated` `dateCreated` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("ALTER TABLE `redemption-code` CHANGE `redeemedAt` `redeemedAt` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("ALTER TABLE `redemption-code` ADD CONSTRAINT `FK_0840600516d67cb748148941a6a` FOREIGN KEY (`redeemedFromCustomerId`) REFERENCES `customer`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-code` ADD CONSTRAINT `FK_8d41fc2b1112d8942908dc1c3c9` FOREIGN KEY (`redeemedFromId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemption-code` ADD CONSTRAINT `FK_990f43507fcc0511a36815fd880` FOREIGN KEY (`redeemedAtOutletId`) REFERENCES `outlet`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `blacklist` CHANGE `date` `date` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("ALTER TABLE `blacklist` DROP COLUMN `token`");
        await queryRunner.query("ALTER TABLE `blacklist` ADD `token` varchar(255) NOT NULL");
    }

}
