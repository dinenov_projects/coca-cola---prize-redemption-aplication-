import { AuthGuard } from '@nestjs/passport';
import { RedemptionRecordService } from './../core/services/redemption-record.service';
import { ShowRedemptionRecordDTO } from './../models/redemption-records/show-redemption-record.dto';
import { Controller, Get, UseGuards, UseFilters } from '@nestjs/common';
import { JwtAuthGuard } from '../common/guards/jwt-auth.guard';
import { EntityNotFoundFilter } from '../common/filters/entity-not-found.filter';
import { BadRequestFilter } from '../common/filters/bad-request.filter';
import { Roles } from '../common/decorators/roles.decorator';
import { RolesIndividualGuard } from '../common/guards/roles-individual.guard';
import { User } from '../common/decorators/user.decorator';
import { UserDTO } from 'src/models/user/user.dto';

@Controller('records')
@UseGuards(AuthGuard(), JwtAuthGuard, RolesIndividualGuard)
@UseFilters(EntityNotFoundFilter, BadRequestFilter)
export class RedemptionRecordController {

    constructor(
        private readonly redemptionRecordService: RedemptionRecordService,
    ) { }

    @Get()
    @Roles('admin')
    async getAllRedemptionRecords(): Promise<ShowRedemptionRecordDTO[]> {
        return this.redemptionRecordService.getAllRedemptionRecords();
    }

    @Get('/personal')
    @Roles('user')
    async getRecords(
        @User() user: UserDTO,
    ): Promise<any> {
        return this.redemptionRecordService.getRecords(user);
    }
}
