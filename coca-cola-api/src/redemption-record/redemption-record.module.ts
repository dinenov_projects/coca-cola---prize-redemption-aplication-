import { AuthModule } from './../auth/auth.module';
import { CoreModule } from './../core/core.module';
import { Module } from '@nestjs/common';
import { RedemptionRecordController } from './redemption-record.controller';

@Module({
    imports: [CoreModule, AuthModule],
    controllers: [RedemptionRecordController],
})
export class RedemptionRecordModule {}
