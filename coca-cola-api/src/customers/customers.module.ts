import { AuthModule } from './../auth/auth.module';
import { Module } from '@nestjs/common';
import { CustomerController } from './customers.controller';
import { CoreModule } from 'src/core/core.module';

@Module({
    imports: [CoreModule, AuthModule],
    controllers: [CustomerController],
})
export class CustomerModule {}
