import { ExceptionFilter, ArgumentsHost, HttpStatus, Catch } from '@nestjs/common';
import { Request, Response } from 'express';
import { EntityNotFoundException } from '../exceptions/entity-not-found';

@Catch(EntityNotFoundException)
export class EntityNotFoundFilter implements ExceptionFilter {
    catch(exception: EntityNotFoundException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();
        const status = HttpStatus.NOT_FOUND;

        const errorResponse = {
        statusCode: status,
        timestamp: new Date().toLocaleDateString(),
        path: request.url,
        method: request.method,
        message: exception.message,
        };

        response.status(status).json(errorResponse);

    }
}
