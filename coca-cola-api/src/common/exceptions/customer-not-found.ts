import { NotFoundException } from '@nestjs/common';

export class CustomerNotFound extends NotFoundException {}
