import { NotFoundException } from "@nestjs/common";

export class RedemptionRecordNotFound extends NotFoundException {}
