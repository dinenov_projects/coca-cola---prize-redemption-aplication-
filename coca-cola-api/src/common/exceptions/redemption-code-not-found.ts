import { NotFoundException } from '@nestjs/common';

export class RedemptionCodeNotFound extends NotFoundException {}
