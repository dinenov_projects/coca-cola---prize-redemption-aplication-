import { BadRequestException } from './bad-request';

export class RedemptionCodeBadRequest extends BadRequestException {}
