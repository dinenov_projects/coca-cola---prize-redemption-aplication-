import { EntityNotFoundException } from './entity-not-found';

export class OutletNotFound extends EntityNotFoundException {}
