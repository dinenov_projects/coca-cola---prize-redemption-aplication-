import { IsString, MinLength, Matches, IsNotEmpty } from 'class-validator';
import { Outlet } from '../../data/entities/outlet.entity';

export class CreateUserDTO {
    @IsString()
    @MinLength(2)
    username: string;

    @IsString()
    @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
    password: string;

    @IsString()
    @MinLength(2)
    firstName: string;

    @IsString()
    @MinLength(2)
    lastName: string;

    @IsString()
    @IsNotEmpty()
    outletId: string;

}
