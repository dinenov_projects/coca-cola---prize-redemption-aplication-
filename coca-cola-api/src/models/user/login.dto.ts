import { IsString, Matches, MinLength } from 'class-validator';

export class LoginDTO {
    @IsString()
    @MinLength(2)
    username: string;

    @IsString()
    @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
    password: string;
}
