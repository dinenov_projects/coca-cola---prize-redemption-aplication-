import { IsString } from 'class-validator';
import { Expose } from 'class-transformer';

export class EditCustomerDTO {
    @IsString()
    @Expose()
    name: string;
}
