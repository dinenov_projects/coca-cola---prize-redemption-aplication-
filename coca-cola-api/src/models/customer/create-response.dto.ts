import { Expose } from 'class-transformer';

export class CreateCustomerResponseDTO {
    @Expose()
    name: string;
    @Expose()
    id: string;
    @Expose()
    dateCreated: Date;
}
