import { Expose } from 'class-transformer';

export class ResponseRedemptionCodeDTO {
    @Expose()
    code: string;
}
