import { RedemptionCodeStatus } from './../../common/enums/redemption-code-status-type';
import { Expose, Type } from 'class-transformer';
import { OutletDTO } from '../outlet/outlet.dto';
import { UserDTO } from '../user/user.dto';
import { CustomerDTO } from '../customer/customer.dto';

export class ShowRedemptionCodeDTO {
    @Expose()
    code: string;

    @Expose()
    status: RedemptionCodeStatus;

    @Expose()
    redeemedAt: Date;

    @Expose({name: '__redeemedAtOutlet__'})
    @Type(() => OutletDTO)
    redeemedAtOutlet: OutletDTO;

    @Expose({name: '__redeemedFrom__'})
    @Type(() => UserDTO)
    redeemedFrom: UserDTO;

    @Expose({name: '__redeemedFromCustomer__'})
    @Type(() => CustomerDTO)
    redeemedFromCustomer: CustomerDTO;

    @Expose()
    dateCreated: Date;

    @Expose()
    reported: boolean;
}
