import { InjectRepository } from '@nestjs/typeorm';
import { Outlet } from '../../data/entities/outlet.entity';
import { Repository } from 'typeorm';
import { OutletDTO } from '../../models/outlet/outlet.dto';
import { plainToClass } from 'class-transformer';
import { CreateOutletDTO } from '../../models/outlet/create-outlet.dto';
import { EditOutletDTO } from '../../models/outlet/edit-outlet.dto';
import { Customer } from 'src/data/entities/customer.entity';
import { OutletNotFound } from 'src/common/exceptions/outlet-not-found';
import { CustomerNotFound } from 'src/common/exceptions/customer-not-found';
import { OutletBadRequest } from 'src/common/exceptions/outlet-bad-request';
import { UsersService } from './users.service';
import { User } from 'src/data/entities/user.entity';

export class OutletService {

    constructor(
        @InjectRepository(Outlet) private readonly outletRepository: Repository<Outlet>,
        @InjectRepository(Customer) private readonly customerRepository: Repository<Customer>,
        private userService: UsersService,
    ) { }

    async getAllOutletsForACustomer(query: any, customerId: string): Promise<OutletDTO[]> {
        let allOutlets: Outlet[];
        if (!query.order) {
            query.order = 'ASC';
        }

        // Outlet search and order
        if (query.order.toLowerCase() === 'desc') {
            allOutlets = await this.outletRepository.find({
                where: { isDeleted: false, customer: { id: customerId, isDeleted: false } },
                relations: ['users', 'customer'],
                order: { name: 'DESC' },
            });
        } else {
            allOutlets = await this.outletRepository.find({
                where: { isDeleted: false, customer: { id: customerId, isDeleted: false } },
                relations: ['customer', 'users'],
                order: { name: 'ASC' },
            });
        }

        // Throw error if no outlets were found for the customer
        if (!allOutlets.toString()) {
            // throw new OutletNotFound('No outlets were found for this customer!');
            return [];
        }

        const outlets = allOutlets.map((outlet: any) => {
            const users = outlet.__users__;
            if (users) {
                const newUsers = users.filter((user: User) => user.isDeleted === false);
                outlet.__users__ = newUsers;
            }
            return outlet;
        });

        // Conversion
        return plainToClass(OutletDTO, outlets, { excludeExtraneousValues: true });
    }

    async getAllOutlets(query: any): Promise<OutletDTO[]> {
        let allOutlets: Outlet[];
        if (!query.order) {
            query.order = 'ASC';
        }

        // Outlet search and order
        if (query.order.toLowerCase() === 'desc') {
            allOutlets = await this.outletRepository.find({
                where: { isDeleted: false },
                relations: ['users', 'customer'],
                order: { name: 'DESC' },
            });
        } else {
            allOutlets = await this.outletRepository.find({
                where: { isDeleted: false },
                relations: ['customer', 'users'],
                order: { name: 'ASC' },
            });
        }

        // Throw error if no outlets were found
        if (!allOutlets.toString()) {
            // throw new OutletNotFound('No outlets were found!');
            return [];
        }

        const outlets = allOutlets.map((outlet: any) => {
            const users = outlet.__users__;
            if (users) {
                const newUsers = users.filter((user: User) => user.isDeleted === false);
                outlet.__users__ = newUsers;
            }
            return outlet;
        });

        // Conversion
        return plainToClass(OutletDTO, outlets, { excludeExtraneousValues: true });
    }

    async getOutletById(id: string): Promise<OutletDTO> {
        const outletId: any = await this.outletRepository.findOne({
            where: { id, isDeleted: false },
            relations: ['users', 'customer'],
        });

        // Throw error if the outlet was not found
        if (!outletId) {
            throw new OutletNotFound('The outlet was not found!');
        }

        const users = outletId.__users__;
        if (users) {
            const newUsers = users.filter((user: User) => user.isDeleted === false);
            outletId.__users__ = newUsers;
        }

        // Conversion
        return plainToClass(OutletDTO, outletId, { excludeExtraneousValues: true });
    }

    async createOutlet(newOutlet: CreateOutletDTO, customerId: string): Promise<OutletDTO | string> {
        const customer = await this.customerRepository.findOne({ id: customerId, isDeleted: false });

        // Throw errors if the customer was not found, or the name and address are taken
        if (
            !customer
        ) {
            throw new CustomerNotFound('The customer was not found!');
        } else if (
            await this.outletRepository.findOne({ address: newOutlet.address })
        ) {
            throw new OutletBadRequest('This address is already registered!');
        } else if (
            await this.outletRepository.findOne({ name: newOutlet.name })
        ) {
            throw new OutletBadRequest('This outlet name is already taken!');
        }

        // Create valid outlet
        const outletToBeCreated = await this.outletRepository.create(newOutlet);
        outletToBeCreated.customer = Promise.resolve(customer);

        const savedOutlet = await this.outletRepository.save(outletToBeCreated);

        // Conversion
        return plainToClass(OutletDTO, savedOutlet, { excludeExtraneousValues: true });
    }

    async editOutlet(id: string, content: EditOutletDTO): Promise<OutletDTO> {
        // Outlet & Customer search
        const outlet: Outlet = await this.outletRepository.findOne({
            where: { id, isDeleted: false },
            relations: ['users', 'customer'],
        });
        // const newCustomer: Customer = await this.customerRepository.findOne({
        //     where: { id: content.customerId, isDeleted: false },
        // });

        // Throw error if the outlet or customer was not found
        if (
            !outlet
        ) {
            throw new OutletNotFound('The outlet was not found!');
        }
        // else if (
        //     !newCustomer
        // ) {
        //     throw new CustomerNotFound('The customer was not found!');
        // }

        // Update outlet
        outlet.name = content.name;
        outlet.address = content.address;
        // outlet.customer = Promise.resolve(newCustomer);
        const savedOutlet = await this.outletRepository.save(outlet);

        // Conversion
        return plainToClass(OutletDTO, savedOutlet, { excludeExtraneousValues: true });
    }

    async deleteOutlet(id: string): Promise<OutletDTO> {
        // Outlet search
        const outlet = await this.outletRepository.findOne({
            where: { id, isDeleted: false },
        });

        // Throw error if the outlet was not found
        if (
            !outlet
        ) {
            throw new OutletNotFound('The outlet was not found!');
        }

        // Change isDeleted field and save in repository
        outlet.isDeleted = true;
        const savedOutlet = await this.outletRepository.save(outlet);

        const users = await outlet.users;
        users.forEach((user: User) => {
            this.userService.deleteUser(user.id);
        });

        // Conversion
        return plainToClass(OutletDTO, savedOutlet, { excludeExtraneousValues: true });
    }
}
