import { LoginDTO } from './../../models/user/login.dto';
import { JwtPayload } from './../interfaces/jwt.payload';
import { UserDTO } from './../../models/user/user.dto';
import { User } from './../../data/entities/user.entity';
import { Injectable } from '@nestjs/common';
import { Repository, Not } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { CreateUserDTO } from 'src/models/user/create-user.dto';
import { EditUserDTO } from '../../models/user/edit-user.dto';
import * as bcrypt from 'bcrypt';
import { UserNotFound } from '../../common/exceptions/user-not-found';
import { UserBadRequest } from '../../common/exceptions/user-bad-request';
import { Outlet } from '../../data/entities/outlet.entity';
import { OutletNotFound } from '../../common/exceptions/outlet-not-found';
import { Roles } from '../../data/entities/roles.entity';

@Injectable()
export class UsersService {

    constructor(
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(Outlet) private readonly outletRepository: Repository<Outlet>,
        @InjectRepository(Roles) private readonly rolesRepository: Repository<Roles>,
    ) { }

    async getAllUsers(): Promise<UserDTO[]> {
        const allUsers = await this.userRepository.find({
            relations: ['customer', 'outlet', 'redemptionCodes'],
            where: {
                isDeleted: false,
                username: Not('Admin'),
            },
            order: {
                username: 'ASC',
            },
        });

        // Throw error if no users were found.
        if (allUsers.length === 0) {
            // throw new UserNotFound('No users were found!');
            return [];
        }

        return allUsers.map(user => plainToClass(UserDTO, user, { excludeExtraneousValues: true }));
    }

    async getUserById(id: string): Promise<UserDTO> {
        const userById = await this.userRepository.findOne({
            where: { isDeleted: false, id },
            relations: ['customer', 'outlet', 'redemptionCodes'],
        });

        // Throw error if user is not found.
        if (!userById) {
            throw new UserNotFound('User not found!');
        }

        return plainToClass(UserDTO, userById, { excludeExtraneousValues: true });
    }

    async createUser(user: CreateUserDTO): Promise<UserDTO> {
        // Check if user with the same username already exists.
        const username = await this.userRepository.findOne({
            where: { username: user.username, isDeleted: false },
            relations: ['customer', 'outlet', 'redemptionCodes'],
        });
        // Search the outlet
        const outlet = await this.outletRepository.findOne({
            where: { id: user.outletId },
        });
        const customer = await outlet.customer;

        // Throw error if user with the same username already exists
        if (username) {
            throw new UserBadRequest('Username already exist.');
        }
        // Throw error if outlet is not found.
        if (!outlet) {
            throw new OutletNotFound('Outlet not found!');
        }

        // Creation of user
        const userToBeCreated = await this.userRepository.create();
        userToBeCreated.username = user.username;
        userToBeCreated.firstName = user.firstName;
        userToBeCreated.lastName = user.lastName;
        userToBeCreated.password = await bcrypt.hash(user.password, 10);
        userToBeCreated.outlet = Promise.resolve(outlet);
        userToBeCreated.customer = Promise.resolve(customer);
        const role = await this.rolesRepository.findOne({
            where: { name: 'user' },
        });
        userToBeCreated.roles = [role];
        const newUser: User = await this.userRepository.save(userToBeCreated);

        // Conversion
        return plainToClass(UserDTO, newUser, { excludeExtraneousValues: true });
    }

    async editUser(id: string, editedUser: EditUserDTO): Promise<UserDTO> {
        // Find the user
        const userToEdit = await this.userRepository.findOne({
            where: { id, isDeleted: false },
            relations: ['customer', 'outlet', 'redemptionCodes'],
        });

        // Throw error if user is not found.
        if (!userToEdit) {
            throw new UserNotFound('User not found!');
        }

        // Change the fields of the edited user object
        userToEdit.username = editedUser.username;
        userToEdit.password = await bcrypt.hash(editedUser.password, 10);
        userToEdit.firstName = editedUser.firstName;
        userToEdit.lastName = editedUser.lastName;

        // Save in user repository
        const savedUser = await this.userRepository.save(userToEdit);

        // Conversion
        return plainToClass(UserDTO, savedUser, { excludeExtraneousValues: true });
    }

    async deleteUser(id: string): Promise<UserDTO> {
        const userForDeletion = await this.userRepository.findOne({
            where: { id, isDeleted: false },
            relations: ['customer', 'outlet', 'redemptionCodes'],
        });

        // Throw error if user is not found
        if (!userForDeletion) {
            throw new UserNotFound('User not found!');
        }

        // Change the isDeleted field to true
        userForDeletion.isDeleted = true;

        // Save in user repository
        const savedUser = await this.userRepository.save(userForDeletion);

        // Convertion
        return plainToClass(UserDTO, savedUser, { excludeExtraneousValues: true });
    }

    async validateUser(payload: JwtPayload): Promise<UserDTO> {
        // Find the user
        const user = await this.userRepository.findOne({
            where: { username: payload.username },
        });

        // Throw error if the user was not found
        if (!user) {
            throw new UserNotFound('You are not registered.');
        }

        return plainToClass(UserDTO, user, { excludeExtraneousValues: true });
    }

    async validatePassword(user: LoginDTO): Promise<boolean> {
        const userEntity = await this.userRepository.findOne({
            where: { username: user.username, isDeleted: false },
        });

        // Throw error if password is missing
        if (!userEntity) {
            throw new UserNotFound('You are not registered.');
        }

        return await bcrypt.compare(user.password, userEntity.password);
    }

    async getUserByUsername(username: string): Promise<UserDTO> {
        const userEntity = await this.userRepository.findOne({
            where: { username },
        });
        return plainToClass(UserDTO, userEntity, { excludeExtraneousValues: true });
    }
}
