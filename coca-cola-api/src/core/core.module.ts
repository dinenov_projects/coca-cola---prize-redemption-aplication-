import { RedemptionRecordService } from './services/redemption-record.service';
import { RedemptionRecord } from './../data/entities/redemption-record.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from '../data/entities/customer.entity';
import { User } from '../data/entities/user.entity';
import { UsersService } from './services/users.service';
import { CustomerService } from './services/customer.service';
import { Outlet } from '../data/entities/outlet.entity';
import { OutletService } from './services/outlet.service';
import { RedemptionCode } from '../data/entities/redemption-code.entity';
import { RedemptionCodeService } from './services/redemption-code.service';
import { Roles } from '../data/entities/roles.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Customer, User, Outlet, RedemptionCode, Roles, RedemptionRecord])],
    providers: [UsersService, CustomerService, OutletService, RedemptionCodeService, RedemptionRecordService],
    exports: [UsersService, CustomerService, OutletService, RedemptionCodeService, RedemptionRecordService],
})
export class CoreModule {}
