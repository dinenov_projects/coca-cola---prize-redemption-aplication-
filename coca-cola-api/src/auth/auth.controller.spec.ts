import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { LoginDTO } from '../models/user/login.dto';
import { UserBadRequest } from '../common/exceptions/user-bad-request';

describe('Auth Controller', () => {
    let controller: AuthController;

    const mockAuthService = {
        login() { ''; },
        logout() { ''; },
    };

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                {
                    provide: AuthService,
                    useValue: mockAuthService,
                },
            ],
            controllers: [AuthController],
        }).compile();

        controller = module.get<AuthController>(AuthController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });

    it('login should return JWT token if correct login information is passed', async () => {
        const correctUserData: LoginDTO = {
            username: 'username',
            password: 'password',
        };
        const token = 'token';

        const loginSpy = jest
            .spyOn(mockAuthService, 'login')
            .mockImplementation(() => Promise.resolve(token));

        const result = await controller.login(correctUserData);

        expect(result).toEqual({ token });
        expect(loginSpy).toHaveBeenCalledTimes(1);

        loginSpy.mockRestore();
    });

    it('login should throw if incorrect login information is passed', async () => {
        try {
            const incorrectUserData: LoginDTO = {
                username: 'username',
                password: 'password',
            };
            const token = 'token';

            const loginSpy = jest
                .spyOn(mockAuthService, 'login')
                .mockImplementation(() => { throw new UserBadRequest('Username or password is incorrect!'); });

            await controller.login(incorrectUserData);

            expect(loginSpy).toHaveBeenCalledTimes(1);

            loginSpy.mockRestore();
        } catch (err) {
            expect(err).toBeInstanceOf(UserBadRequest);
        }
    });

    it('logout should return the blacklisted JWT token', async () => {

        const token = 'token';

        const logoutSpy = jest
            .spyOn(mockAuthService, 'logout')
            .mockImplementation(() => token);

        const result = await controller.logout(token);

        expect(result).toEqual({token});
        expect(logoutSpy).toHaveBeenCalledTimes(1);

        logoutSpy.mockRestore();
    });
});
