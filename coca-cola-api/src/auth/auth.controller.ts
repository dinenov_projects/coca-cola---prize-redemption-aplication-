import { EntityNotFoundFilter } from './../common/filters/entity-not-found.filter';
import { BadRequestFilter } from './../common/filters/bad-request.filter';
import { AuthService } from './auth.service';
import { LoginDTO } from './../models/user/login.dto';
import { Controller, Body, ValidationPipe, Post, Delete, UseGuards, UseFilters } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Token } from '../common/decorators/token.decorator';
import { JwtAuthGuard } from '../common/guards/jwt-auth.guard';

@UseFilters(BadRequestFilter, EntityNotFoundFilter)
@Controller('session')
export class AuthController {

    constructor(private readonly authService: AuthService) {}

    @Post()
    async login(
        @Body(new ValidationPipe({ whitelist: true, transform: true }))
        loginContent: LoginDTO,
    ): Promise<{token: string}> {
        const token = await this.authService.login(loginContent);
        return {token};
    }

    @Delete()
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard)
    async logout(
        @Token() token: string,
    ): Promise<{token: string}> {
        const blacklistedToken = await this.authService.logout(token);
        return {token: blacklistedToken};
    }
}
