import { AuthModule } from './../auth/auth.module';
import { CoreModule } from 'src/core/core.module';
import { UserController } from './user.controller';
import { Module } from '@nestjs/common';

@Module({
    imports: [CoreModule, AuthModule],
    controllers: [UserController],
})
export class UserModule {}
