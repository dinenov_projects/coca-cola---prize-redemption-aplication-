# COCA-COLA - (Prize Redemption Application) - API

<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

<p align="center">A progressive <a href="http://nodejs.org" target="blank">Node.js</a> framework for building efficient and scalable server-side applications, heavily inspired by <a href="https://angular.io" target="blank">Angular</a>.</p>

## Description

API applicaton for validation of redemption codes assigned by Telerik Academy and Coca-Cola Company. The application have one administrator which is the owner of the system. The users of this application are the employees of certain Coca-Cola customers. Customers are physical stores selling the products to the end consumers.
The application was made with NestJS framework.

The server is hosted in Heroku for demonstration purposes:
[coca-cola-backend](https://coca-cola-backend.herokuapp.com/)

## API Functions:

### Administrator can:
- CRUD Customers
- CRUD Outlets
- CRUD Users
- See all redemption codes
- See all redemption records
### Users can:
- Validate redemption codes
- Report if there is attempt for redemption of already redeemed code.
### Authentication: 
- Login/Logout for Users
- Login/Logout for Admin
- Blacklist for JW Tokens

### Technologies:
- ORM - TypeORM
- Database - MariaDB
- REST API - NestJS

# Getting started

## Installation

```bash
$ npm install
```

## Configuration

### Example .env file
```bash
PORT=3000
JWT_SECRET=example
DB_TYPE=mysql
DB_HOST=localhost
DB_PORT=3306
DB_USERNAME=example_username
DB_PASSWORD=example_password
DB_DATABASE_NAME=example_database_name
```

### Example of ormconfig.json according to .env file
- ormconfig.json should be configured like so:
```bash
{
  "type": "mysql",
  "host": "localhost",
  "port": 3306,
  "username": "example_username",
  "password": "example_password",
  "database": "example_database_name",
  "synchronize": false,
  "logging": false,
  "entities": [
    "./src/data/entities/**/*.ts"
  ],
  "migrations": [
    "./src/data/migration/**/*.ts"
  ],
  "subscribers": [
    "./src/common/subscribers/*.subscriber.ts"
  ],
  "cli": {
    "entitiesDir": "./src/data/entities",
    "migrationsDir": "./src/data/migration"
  }
}
```

## Running the app

```bash
# run migration
$ npm run typeorm migration:run

# seed
$ npm run seed

# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```

# Usage of the application and return objects


## CRUD operations for Customers (working in development mode)

## Create Customer
- On route localhost:3000/customers, Post request method with body:
```bash
{
  "name": "string"
}
```
### Return Object
```bash
{
  "name": "string",  
  "id": "uuid",
  "dateCreated": "Date"
}
```

## Edit Customer
- On route localhost:3000/customers/id, Put request method with body:
```Bash
{
  "name": "string"
}
```
### Return Object
```Bash
{
  "name": "string"
}
```

## Delete Customer
- On route localhost:3000/customers/id, Delete request method 
### Return Object
```Bash
{
  "id": "string",
  "name": "string",
  "dateCreated": "Date",
  "outlets": [/*Outlet object*/]
}
```

## Show all Customers
- on On route localhost:3000/customers, Get request method
### Return Object
```Bash
[
  {
    "id": "string",
    "name": "string",
    "dateCreated": "Date",
    "outlets": [/*Outlet object*/]
  },
]
```

## Show Customers by ID
- on On route localhost:3000/customers/id, Get request method
### Return Object
```Bash 
{
  "id": "string",
  "name": "string",
  "dateCreated": "Date",
  "outlets": [/*Outlet object*/]
}
```


## CRUD operations for Outlets (working in development mode)

## Create Outlet
- On route localhost:3000/customers/:customerId/outlets, Post request method with body:
```Bash 
{
  "name": "string",
  "address": "string"
}
```
### Return Object
- Outlet object
```Bash
{
  "id": "string",
  "address": "string",
  "customer": {
    "id": "string",
    "name": "string",
    "dateCreated": "Date",
    "outlets": [/*Outlet object*/]
  },
  "users": [
    {
      "id": "string",
      "username": "string",
      "firstName": "string",
      "lastName": "string",
      "customer": {
        "id": "string",
        "name": "string",
        "dateCreated": "Date",
        "outlets": [/*Outlet object*/]
      },
      "outlet": /*Outlet object*/,
      "redemptionCodes": {
        "code": "string"
      } ,
      "dateCreated": "Date",
      "roles": {
        "name": "string"
      }
    },
  ],
  "dateCreated": "Date"
}
or 
"string"
```

## Edit Outlet
- On route localhost:3000/customers/:customerId/outlets/:id or localhost:3000/outlets/:id, Put request method with body:
```Bash
{
  "name": "string",
  "address": "string"
}
```
### Return Object
- Outlet object
```Bash
{
  "id": "string",
  "address": "string",
  "customer": {
    "id": "string",
    "name": "string",
    "dateCreated": "Date",
    "outlets": [/*Outlet object*/]
  },
  "users": [
    {
      "id": "string",
      "username": "string",
      "firstName": "string",
      "lastName": "string",
      "customer": {
        "id": "string",
        "name": "string",
        "dateCreated": "Date",
        "outlets": [/*Outlet object*/]
      },
      "outlet": /*Outlet object*/,
      "redemptionCodes": {
        "code": "string"
      } ,
      "dateCreated": "Date",
      "roles": {
        "name": "string"
      }
    },
  ],
  "dateCreated": "Date"
}
```

## Delete Outlet
- On route localhost:3000/outlets/:id or localhost:3000/customers/:customerId/outlets/:id, Delete request method
### Return Object
- Outlet object
```Bash
{
  "id": "string",
  "address": "string",
  "customer": {
    "id": "string",
    "name": "string",
    "dateCreated": "Date",
    "outlets": [/*Outlet object*/]
  },
  "users": [
    {
      "id": "string",
      "username": "string",
      "firstName": "string",
      "lastName": "string",
      "customer": {
        "id": "string",
        "name": "string",
        "dateCreated": "Date",
        "outlets": [/*Outlet object*/]
      },
      "outlet": /*Outlet object*/,
      "redemptionCodes": {
        "code": "string"
      } ,
      "dateCreated": "Date",
      "roles": {
        "name": "string"
      }
    },
  ],
  "dateCreated": "Date"
}
```

## Show all Outlets
- On route localhost:3000/outlets with optional query parameters, Get request method
### Return Object
- Outlet object
```Bash
[
  {
    "id": "string",
    "address": "string",
    "customer": {
      "id": "string",
      "name": "string",
      "dateCreated": "Date",
      "outlets": [/*Outlet object*/]
    },
    "users": [
      {
        "id": "string",
        "username": "string",
        "firstName": "string",
        "lastName": "string",
        "customer": {
          "id": "string",
          "name": "string",
          "dateCreated": "Date",
          "outlets": [/*Outlet object*/]
        },
        "outlet": /*Outlet object*/,
        "redemptionCodes": {
          "code": "string"
        } ,
        "dateCreated": "Date",
        "roles": {
          "name": "string"
        }
      },
    ],
    "dateCreated": "Date"
  },
]
```

## Show Outlets for Customer ID
- On route localhost:3000/customers/:customerId/outlets, Get request method
### Return Object
- Outlet object
```Bash
[
  {
    "id": "string",
    "address": "string",
    "customer": {
      "id": "string",
      "name": "string",
      "dateCreated": "Date",
      "outlets": [/*Outlet object*/]
    },
    "users": [
      {
        "id": "string",
        "username": "string",
        "firstName": "string",
        "lastName": "string",
        "customer": {
          "id": "string",
          "name": "string",
          "dateCreated": "Date",
          "outlets": [/*Outlet object*/]
        },
        "outlet": /*Outlet object*/,
        "redemptionCodes": {
          "code": "string"
        } ,
        "dateCreated": "Date",
        "roles": {
          "name": "string"
        }
      },
    ],
    "dateCreated": "Date"
  },
]
```

## Show Outlet by ID
- On route localhost:3000/customers/:customerId/outlets/:id or localhost:3000/outlets/:id, Get request method
### Return Object
- Outlet object
```Bash
{
  "id": "string",
  "address": "string",
  "customer": {
    "id": "string",
    "name": "string",
    "dateCreated": "Date",
    "outlets": [/*Outlet object*/]
  },
  "users": [
    {
      "id": "string",
      "username": "string",
      "firstName": "string",
      "lastName": "string",
      "customer": {
        "id": "string",
        "name": "string",
        "dateCreated": "Date",
        "outlets": [/*Outlet object*/]
      },
      "outlet": /*Outlet object*/,
      "redemptionCodes": {
        "code": "string"
      } ,
      "dateCreated": "Date",
      "roles": {
        "name": "string"
      }
    },
  ],
  "dateCreated": "Date"
}
```


## CRUD operations for Users (working in development mode)

## Create User
- On route localhost:3000/users, Post request method with body:
```Bash
{
  "username": "string",
  "password": "string",
  "firstName": "string",
  "lastName": "string",
  "outletId": "string"
}
```
### Return Object
- User object
```Bash 
{
  "id": "string",
  "username": "string",
  "firstName": "string",
  "lastName": "string",
  "customer": {
    "id": "string",
    "name": "string",
    "dateCreated": "Date",
    "outlets": [/*Outlet object*/]
  },
  "outlet": /*Outlet object*/,
  "redemptionCodes": {
    "code": "string"
  } ,
  "dateCreated": "Date",
  "roles": {
    "name": "string"
  }
}
```

## Edit User
- On route localhost:3000/users/id, Put request method with body:
```Bash 
{
  "username": "string",
  "password": "string",
  "firstName": "string",
  "lastName": "string"
}
```
### Return Object
- User object
```Bash 
{
  "id": "string",
  "username": "string",
  "firstName": "string",
  "lastName": "string",
  "customer": {
    "id": "string",
    "name": "string",
    "dateCreated": "Date",
    "outlets": [/*Outlet object*/]
  },
  "outlet": /*Outlet object*/,
  "redemptionCodes": {
    "code": "string"
  } ,
  "dateCreated": "Date",
  "roles": {
    "name": "string"
  }
}
```

## Delete User
- On route localhost:3000/users/id, Delete request method 
### Return Object
- User object
```Bash 
{
  "id": "string",
  "username": "string",
  "firstName": "string",
  "lastName": "string",
  "customer": {
    "id": "string",
    "name": "string",
    "dateCreated": "Date",
    "outlets": [/*Outlet object*/]
  },
  "outlet": /*Outlet object*/,
  "redemptionCodes": {
    "code": "string"
  } ,
  "dateCreated": "Date",
  "roles": {
    "name": "string"
  }
}
```

## Show all Users
- On route localhost:3000/users, Get request method
### Return Object
- User object
```Bash 
[
  {
    "id": "string",
    "username": "string",
    "firstName": "string",
    "lastName": "string",
    "customer": {
      "id": "string",
      "name": "string",
      "dateCreated": "Date",
      "outlets": [/*Outlet object*/]
    },
    "outlet": /*Outlet object*/,
    "redemptionCodes": {
      "code": "string"
    } ,
    "dateCreated": "Date",
    "roles": {
      "name": "string"
    }
  },
]
```

## Show Users by ID
- On route localhost:3000/users/id, Get request method
### Return Object
- User object
```Bash 
{
  "id": "string",
  "username": "string",
  "firstName": "string",
  "lastName": "string",
  "customer": {
    "id": "string",
    "name": "string",
    "dateCreated": "Date",
    "outlets": [/*Outlet object*/]
  },
  "outlet": /*Outlet object*/,
  "redemptionCodes": {
    "code": "string"
  } ,
  "dateCreated": "Date",
  "roles": {
    "name": "string"
  }
}
```

## Redemption code

## Show all Redemption Codes
- On route localhost:3000/codes, Get request method
### Return Object
```Bash
[
  {
  "code": "string",
  "status": " 'valid' or 'redeemed' or 'canceled' or 'invalid' ",
  "redeemedAt": "Date",
  "redeemedAtOutlet": /*Outlet object*/,
  "redeemedFrom": /*User object*/,
  "redeemedFromCustomer": {
    "id": "string",
    "name": "string",
    "dateCreated": "Date",
    "outlets": /*Outlet object*/
  },
  "dateCreated": "Date",
  "reported": "boolean"
  },
]
```

## Show Redemption Codes by ID
- On route localhost:3000/codes/id, Get request method
### Return Object
```Bash
{
  "code": "string",
  "status": " 'valid' or 'redeemed' or 'canceled' or 'invalid' ",
  "redeemedAt": "Date",
  "redeemedAtOutlet": /*Outlet object*/,
  "redeemedFrom": /*User object*/,
  "redeemedFromCustomer": {
    "id": "string",
    "name": "string",
    "dateCreated": "Date",
    "outlets": /*Outlet object*/
  },
  "dateCreated": "Date",
  "reported": "boolean"
}
```

## Update status of Redemption Code
- On route localhost:3000/codes/id, Put request method with body:
```Bash
{
  "status": " 'valid' or 'redeemed' or 'canceled' or 'invalid' "
}
```
### Return Object
```Bash
{
  "code": "string",
  "status": " 'valid' or 'redeemed' or 'canceled' or 'invalid' ",
  "redeemedAt": "Date",
  "redeemedAtOutlet": /*Outlet object*/,
  "redeemedFrom": /*User object*/,
  "redeemedFromCustomer": {
    "id": "string",
    "name": "string",
    "dateCreated": "Date",
    "outlets": /*Outlet object*/
  },
  "dateCreated": "Date",
  "reported": "boolean"
}
```

## Report Redemption Code
- On route localhost:3000/codes/id/report, Put request method
### Return Object
```Bash
{
  "code": "string",
  "status": " 'valid' or 'redeemed' or 'canceled' or 'invalid' ",
  "redeemedAt": "Date",
  "redeemedAtOutlet": /*Outlet object*/,
  "redeemedFrom": /*User object*/,
  "redeemedFromCustomer": {
    "id": "string",
    "name": "string",
    "dateCreated": "Date",
    "outlets": /*Outlet object*/
  },
  "dateCreated": "Date",
  "reported": "boolean"
}
```


## Redemption Records

## Show all Redemption Records
- On route localhost:3000/records, Get request method
### Return Object
```Bash
[
  {
    "redemptionCode": {
      "code": "string",
      "status": " 'valid' or 'redeemed' or 'canceled' or 'invalid' ",
      "redeemedAt": "Date",
      "redeemedAtOutlet": /*Outlet object*/,
      "redeemedFrom": /*User object*/,
      "redeemedFromCustomer": {
        "id": "string",
        "name": "string",
        "dateCreated": "Date",
        "outlets": /*Outlet object*/
      },
      "dateCreated": "Date",
      "reported": "boolean"
    },
    "updatedByUser": {
      "id": "string",
      "username": "string",
      "firstName": "string",
      "lastName": "string",
      "customer": {
        "id": "string",
        "name": "string",
        "dateCreated": "Date",
        "outlets": [/*Outlet object*/]
      },
      "outlet": /*Outlet object*/,
      "redemptionCodes": {
        "code": "string"
      } ,
      "dateCreated": "Date",
      "roles": {
        "name": "string"
      }
    },
    "customer": {
      "id": "string",
      "name": "string",
      "dateCreated": "Date",
      "outlets": /*Outlet object*/
    },
    "outlet": /*Outlet object*/,
    "dateCreated": "Date";
    "status": " 'valid' or 'redeemed' or 'canceled' or 'invalid' ",
    "info": "string"
  },
]
```